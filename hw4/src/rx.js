class Stream {
    constructor() {
        this.callbacks = [];
    }

    // Complete the Stream class here.
    subscribe(f) {
        this.callbacks.push(f)
    }

    _push(value) {
        for(var i = 0; i < this.callbacks.length; i++) {
            this.callbacks[i](value);
        }
    }

    _push_many(arr) {
        for(var i = 0; i < arr.length; i++) {
            this._push(arr[i]);
        }
    }

    first() {
        var newObj = new Stream();
        newObj.valuesRemaining = 1;
        this.callbacks.push(function(value) {
            if(newObj.valuesRemaining === 0) {
                return;
            }
            newObj.valuesRemaining--;
            newObj._push(value);
        })
        return newObj;
    }

    map(f) {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            newObj._push(f(value));
        })
        return newObj;
    }

    filter(f) {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            if(f(value)) {
                newObj._push(value);
            }
        })
        return newObj;
    }

    distinctUntilChanged() {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            if (newObj.lastVal === value) {
                return;
            }
            newObj.lastVal = value;
            newObj._push(value);
        })
        return newObj;
    }

    flatten() {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            if (Array.isArray(value)) {
                newObj._push_many(value);
            } else {
                newObj._push(value);
            }
        })
        return newObj;
    }

    scan(f, acc) {
        var newObj = new Stream();
        newObj.accum = acc;
        this.callbacks.push(function(value) {
            value = f(newObj.accum, value);
            newObj.accum = value;
            newObj._push(value);
        })
        return newObj;
    }

    join(other) {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            newObj._push(value);
        })
        other.callbacks.push(function(value) {
            newObj._push(value);
        })
        return newObj;
    }

    combine() {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            value.subscribe(function(value) {
                newObj._push(value);
            })
        })
        return newObj;
    }

    zip(other, f) {
        var newObj = new Stream();
        this.callbacks.push(function(value) {
            newObj.firstzip = value;
            if(newObj.secondzip !== undefined) {
                value = f(newObj.firstzip, newObj.secondzip);
                newObj._push(value);
            }
        })
        other.callbacks.push(function(value) {
            newObj.secondzip = value;
            if(newObj.firstzip !== undefined) {
                value = f(newObj.firstzip, newObj.secondzip);
                newObj._push(value);
            }
        })
        return newObj;
    }

    throttle(n) {
        var newObj = new Stream();
        newObj.enabled = true;
        this.callbacks.push(function(value) {
            if(newObj.enabled === true) {
                newObj._push(value);
                newObj.enabled = false;
                setTimeout(function() {
                    newObj.enabled = true;
                }, n)
            }
        })
        return newObj;
    }

    latest() {
        var newObj = new Stream();
        this.callbacks.push(function(stream) {
            if (stream === undefined) {
                return;
            }
            newObj.newStream = stream;
            stream.subscribe(function(value) {
                if(stream === newObj.currentStream) {
                    newObj._push(value);
                } else if (stream === newObj.newStream) {
                    newObj.currentStream = stream;
                    newObj._push(value);
                }
            })
        })
        return newObj;
    }

    unique(f) {
        var newObj = new Stream();
        // Uses object properties as a dictionary with their value as
        // key and true as their value.
        newObj.pastValues = {};
        this.callbacks.push(function(value) {
            if (!newObj.pastValues.hasOwnProperty(f(value))) {
                // Only executes if the pastValues object does not have
                // this property.
                newObj._push(value);
                newObj.pastValues[f(value)] = true;
            }
        })
        return newObj;
    }

    static timer(n) {
        var newObj = new Stream();
        setInterval(function(){
            var currentTime = new Date();
            newObj._push(currentTime);
        }, n);
        return newObj;
    }

    static dom(element, eventname) {
        var newObj = new Stream();
        element.on(eventname, function(eventObject) {
            newObj._push(eventObject);
        });
        return newObj;
    }

    static url(url) {
        var newObj = new Stream();
        $.get(url, function(value) {
            newObj._push(value);
        }, "json");
        return newObj;
    }

    // *** NEW ***
    static ajax(_url, _data) {
        var out = new Stream();
        $.ajax({
            url: _url,
            dataType: "jsonp",
            jsonp: "callback",
            data: _data,
            success: function(data) { out._push(data[1]); }
        });
        return out;
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    // *** NEW ***
    function WIKIPEDIAGET(s) {
        return Stream.ajax("https://en.wikipedia.org/w/api.php?format=json", { action: "opensearch", search: s, namespace: 0, limit: 10 });
    }

    // Add your hooks to implement Parts 2-4 here.
    var timerStream = Stream.timer(100);
    timerStream.subscribe(function(value) {
        $("#time").text(value.toString());
    })

    var buttonStream = Stream.dom($("#button"), "click");
    var timesClicked = 0;
    buttonStream.subscribe(function(value) {
        timesClicked++;
        $("#clicks").text(timesClicked);
    })

    var mousePositionStream = Stream.dom($("#mousemove"), "mousemove");
    var throttledStream = mousePositionStream.throttle(1000);
    throttledStream.subscribe(function(value) {
        $("#mouseposition").text("x: " + value.clientX + ", y: " + value.clientY);
    })

    var firearr = [];
    Stream.timer(60000).map(function(n) {
        return Stream.url(FIRE911URL);
    }).combine().flatten().unique(function(value) {
        return value.id;
    }).subscribe(function(value) {
        $("#fireevents").append($("<li></li>").text(value["405818852"]));
        firearr.push(value);
    })

    Stream.dom($("#wikipediasearch"), "input").throttle(100).map(function(value) {
        return WIKIPEDIAGET($("#wikipediasearch")[0].value)
    }).latest().subscribe(function(value) {
        if(value === undefined) {
            return;
        }
        $("#wikipediasuggestions").html("");
        for (var i = 0; i < value.length; i++) {
            $("#wikipediasuggestions").append($("<li></li>").text(value[i]));
        }
    });

    Stream.dom($("#firesearch"), "input").subscribe(function(event) {
        $("#fireevents").empty();
        // Go through each element and see if they are a substring. Made it case insensitive becuase
        // that is what most searches are.
        for (var i = 0; i < firearr.length; i++) {
            if (firearr[i]["405818852"].toUpperCase().includes(event.target.value.toUpperCase())) {
                $("#fireevents").append($("<li></li>").text(firearr[i]["405818852"]));
            }
        }
    })

    var finishedLoadingStream = Stream.dom($("#load-complete"), "click");
    var finishedLoading = false;
    var timesLoaded = 0;
    var loadedSize = 10;

    Stream.dom($("#load-more"), "click").join(finishedLoadingStream).subscribe(function(event) {
        if (event.currentTarget.id === "load-complete") {
            $("#progress").attr("style", 'width:100%; background-color:Lime;">');
            finishedLoading = true;
        } else if (!finishedLoading) {
            // Could use Accum here but this will be easier to debug and maintain. Also
            // has less overhead when compared to a stream.
            timesLoaded++;
            if (timesLoaded === loadedSize) {
                loadedSize *= 2;
            }

            $("#progress").attr("style", 'width:' + Math.round(timesLoaded * 100.0 / loadedSize) + '%;">');
        }
    })
}
